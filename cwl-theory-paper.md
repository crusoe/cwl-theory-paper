Outline:

Goals of the CWL project
1. standardized functional portable description of command line tools and dataflow workflows made up of them
1b. these descriptions are complete enough to execute
2. and therefore an ecosystem with many implementation. 
3. improve communication and understanding between workflow author and users
   labels / human friendly identifiers / subworflows
4. That data and inputs are explicit and they have identifiers

Anti-goals
1. (web) service orchestration / interaction with external stateful systems
  Why not? they have state, can go away, not idempotent, not reproducible. (The Taverna experience)
  Services are not bad, but they need to give the users the workflow and references to the reference data used
2. business process management nor other control-flow approaches
  (no stopping for external decision making, which is not reproducible). CWL tools/workflows could be called from a business process management.
3. not a strict guarentee of reproducibility

Design choices

(for each,
 1. say "why", linking to one of the goals
 2. the intuition, the "what"
 3. the benefit
 4. how this is accomplished, possibly with a running example
 5. alternatives not chosen, and why they weren't selected

See also https://github.com/joelparkerhenderson/architecture_decision_record/blob/master/adr_template_by_jeff_tyree_and_art_akerman.md )

- Unit of compute (POSIX command line tools)
a. portability
- Unit of data is the filesystem and strings
- connection between compute & data into a full workflow (the dataflow model)
- file streaming is supported
- no tool to tool IP based communication
- Syntax choices (balance between readability and using off the shelf libraries for parsing)
- workflow constructs supported (crossreference the workflow patterns repository)
- workflow constructs purposely not supported & why
- which parts are optional
- linked data & external ontologies for i/o formats & tool/workflow description metadata
- software container support via docker format containers (but supporting other engines)
- mechanism for vendor extensions
- the conformance tests
- Optional support for Javascript in very well defined circumstances
- seperation of concerns
 - supporting single-machine, single-site, and distributed execution
 - data references via URI, not filepath

<!-- Do we need a comparision with specific workflow langs/systems? Maybe just a table -->

Prior art:
- Wf4Ever
- ...

Relationship with control-flow workflows (systems):
(data-flow workflows go inside control-flow workflows, not the other way around)


---
Do any of the below go in this paper?

How does CWL relate to ASAP
- Automation
- Scaling
- Abstraction
- Proveance

How does CWL relate to the 17 FAIR principles?

Snapshot of the CWL ecosystem in 2020
- editor plugins
- workflow transformation
- use of just the command line tool standard

Future topics:
- Spark+CWL: add annotations so that workflow executors with access to Spark clusters can use them without having to modify the tools or workflows
- syntax simplifications: moving inputBinding to the "arguments" section and outputBindings to a new "results" section.
- performance portability (with an bio- example)
- other non-functional requirements
